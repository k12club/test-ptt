var express = require('express');
var router = express.Router();
var soap = require('soap');
var parseString = require('xml2js').parseString;
var transform = require('../../services/date.service')
/* GET users listing. */
router.get('/', function (req, res, next) {
    // เชื่อมต่อ SOAP ของ PTT เพื่อดึงราคาน้ำมัน
    var url = 'https://www1.pttor.com/OilPrice.asmx?op=CurrentOilPrice&wsdl';
    var args = { Language: 'th' };
    try {
        soap.createClient(url, function (err, client) {
            client.CurrentOilPrice(args, function (err, result) {

                if (result.CurrentOilPriceResult) {
                    let newString = result.CurrentOilPriceResult.replace(/\\r\\n/g, '$1');
                    // console.log(newString);
                    parseString(newString, function (err, result2) {
                        // res.json(result2.PTTOR_DS.FUEL)
                        var reformattedArray = result2.PTTOR_DS.FUEL
                            .filter(res => res.PRICE && res.PRODUCT)
                            .map(obj => ({ MAT_NAME: obj.PRODUCT[0], date: obj.PRICE_DATE[0], PRICE0: obj.PRICE[0] }));

                        let t = [{ time: 'numeric' }]
                        let updateDate = new Date(result2.PTTOR_DS.FUEL[0].PRICE_DATE[0])
                        let ChangeTime = (updateDate.getHours().toString().length == 1 ? '0' : '') + updateDate.getHours() + ":" + (updateDate.getMinutes().toString().length == 1 ? '0' : '') + updateDate.getMinutes()
                        // console.log(s);
                        res.json({
                            code: 100,
                            Description: transform(result2.PTTOR_DS.FUEL[0].PRICE_DATE[0], 'full'),
                            ChangeDate: join(updateDate, t, ' '),
                            ChangeTime,
                            ListOfPrice: reformattedArray,
                            // date
                        })
                    });
                }

            });
        });
    } catch (err) {
        console.log(err);

    }
});
function join(t, a, s) {
    function format(m) {
        let f = new Intl.DateTimeFormat('th', m);
        return f.format(t);
    }
    return a.map(format).join(s);
}


module.exports = router;
