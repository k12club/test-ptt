const checkPrime = num => {
    for (let i = 2, s = Math.sqrt(num); i <= s; i++)
        if (num % i === 0) return false;
    return num > 1;
}
const question1_1 = () => {
    let numbers = 1000000
    let arr = []
    for (let i = 1; i < numbers; i++) {
        checkPrime(i) ? arr.push(i) : null
    }
    console.log('====================================');
    console.log({ all_prime: arr.toString(), sum_prime: arr.length });
    console.log('====================================');
    // return checkPrime(9)
}
question1_1()
const question1_2 = () => {
    function factorial(n) {
        let answer = 1;
        if (n == 0 || n == 1) {
            return answer;
        } else {
            for (var i = n; i >= 1; i--) {
                answer = answer * i;
            }
            return answer;
        }
    }
    let number = 100;
    answer = factorial(number)
    console.log(`factorial of ${number} is ${answer}`);
}
question1_2()

const question1_3 = () => {
    let number = 1000
    for (let i = 1; i < number; i++) {
        let arr = new Array(i)
        console.log(arr.join('1'));
    }
}
question1_3()

